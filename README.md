# Catan Card Game

This repository contains assets for the game "De Kolonisten van Catan: Het Kaartspel voor Twee Spelers", which is a card game made by Klaus Teuber. The game was released in the Netherlands and Belgium.

## Contributing

You are welcome to make pull requests for missing assets, if you have images of better quality.

Pull requests for images of a different size of image type are also welcome.
